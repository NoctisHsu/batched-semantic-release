# batched-semantic-release

Test project showing how to semantically publish multiple commits
to NPM from GitLab

Uses [semantic-release-gitlab](https://gitlab.com/hutson/semantic-release-gitlab)

To publish to NPM use semantic commits and then tag with string that
starts with `release-`.

See [.gitlab-ci.yml](.gitlab-ci.yml) for actual setup.

## Example

This repo has multiple commits at the beginning.

```sh
git commit -m "feat(code): test feature 1"
git push origin master
git commit -m "feat(code): test feature 2"
git push origin master
git commit -m "feat(code): test feature 3"
git push origin master
```

I pushed the commits and the GitLab CI
But only the "test" job has executed, not the "deploy" one.
Now let us publish this project to NPM by tagging a commit
with "release-" string. I prefer using the date

```sh
git tag release-2016-08-30
```

The "deploy" job runs and publishes a version of this package to NPM

```sh
npm http 200 https://registry.npmjs.org/batched-semantic-release
+ batched-semantic-release@1.0.0
```

Now add another commit `feat(tag): describe tag example` and release it

```sh
git tag release-again
```
